def count
  seconds = ARGV[0].to_i
  if seconds < 1
    print "Error! Your value is not Integer or less than 1"
    exit
  else
    print "Timer started\n"
    seconds.times do |sec|
      seconds = seconds - 1
      puts "#{seconds + 1} seconds left"
      sleep 1
    end
  end
  print "Time is up!"
end

count