#we adding few arrays: chars, numbers, symbols, vowels. We want to operate on them
# then we add them to the empty @result array
@result   = []
@chars    = ('a'..'z').to_a
@vowels   = %w[a e i o u]
@numbers  = (0..9).to_a
@symbols  = %w[! @ # $ % ^ & *  _ - + = < > ?]

# ARGV[0] just is takes the first value after the name of the script. 
# This value will be a length of our password.
@length   = ARGV[0].to_i

def generate_random
  # in this part we "mixing up" chars, numbers and symbols and adding them to the @result array
  @result.concat(@chars.shuffle)
  @result.concat(@numbers.shuffle)
  @result.concat(@symbols.shuffle)
  
  password = @result.shuffle.take(@length).join
  puts password
end

def generate_memorable
  consonants = @chars + @vowels
  # we creating an array with consonants, first we join @chars and @vowels together and then 
  # we remove duplicates, so there are only consonants left
  consonants.each do |el|
    if consonants.count(el) > 1
      consonants.delete(el)
    end
  end

  # in this part we iterate all the consonants and add to each consonant the vowel
  consonants.each do |consonant|
    @result << consonants.sample
    @result << @vowels.sample
  end

  password = @result.take(@length).join
  puts password
end

generate_memorable